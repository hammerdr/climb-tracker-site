Rails.application.routes.draw do
  resources :climbs, only: [:index, :create, :show, :destroy]

  get 'health_check' => 'health_check#index'
end
