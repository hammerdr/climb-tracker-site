class ClimbsController < ApplicationController
  skip_before_action :verify_authenticity_token
  def create
    climb = Climb.create climb_params
    respond_to do |format|
      format.json { render json: climb }
      format.html { redirect_to action: :index }
    end
  end

  def index
    @climbs = Climb.all
    respond_to do |format|
      format.json { render json: { climbs: @climbs } }
      format.html
    end
  end

  def destroy
    Climb.delete(params[:id])
    respond_to do |format|
      format.json { render json: { climbs: Climb.all } }
      format.html { redirect_to action: :index }
    end
  end

  private

  def climb_params
    params.require(:climb).permit(:date, :rating, :climb_type)
  end
end
