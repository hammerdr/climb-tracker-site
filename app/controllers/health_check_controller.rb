class HealthCheckController < ApplicationController
  def index
    render json: { healthy: true, message: 'all good' }
  end
end
