class AddClimbTypeToClimbs < ActiveRecord::Migration
  def change
    add_column :climbs, :climb_type, :string
  end
end
