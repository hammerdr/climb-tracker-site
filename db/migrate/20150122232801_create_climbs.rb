class CreateClimbs < ActiveRecord::Migration
  def change
    create_table :climbs do |t|
      t.datetime :date
      t.string :rating

      t.timestamps null: false
    end
  end
end
