require 'rails_helper'

describe 'climbs/index' do
  before do
    @a_time = DateTime.new(1990, 5, 30)
    first_climb = Climb.new(date: @a_time, rating: '5.10a')
    second_climb = Climb.new(date: DateTime.new, rating: '5.10b')
    assign :climbs, [first_climb, second_climb]
  end
  subject { render; rendered }
  it { is_expected.to have_css('.climb', count: 2) }
  it { is_expected.to have_css('.climb h4', text: '5.10a') }
  it { is_expected.to have_css('.climb h4', text: '5.10b') }
  it { is_expected.to have_css('.climb h3', text: 'May 30th, 1990 00:00') }
end
