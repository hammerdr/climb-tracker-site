require 'rails_helper'

describe 'routes' do
  it 'should route GET health_check to health_check#index' do
    expect(get: '/health_check').to route_to('health_check#index')
  end
  it 'should route GET climbs to climbs#index' do
    expect(get: '/climbs').to route_to('climbs#index')
  end
  it 'should route POST climbs to climbs#create' do
    expect(post: '/climbs').to route_to('climbs#create')
  end
  it 'should route GET climbs/123 to climbs#show id: 123' do
    expect(get: '/climbs/123').to route_to('climbs#show', id: "123")
  end
end
