require 'rails_helper'

RSpec.describe ClimbsController, :type => :controller do
  describe 'GET index' do
    let!(:first_climb) { Climb.create(date: DateTime.now, rating: '5.10a') }
    let!(:second_climb) { Climb.create(date: DateTime.now, rating: '5.10b') }

    it 'should render index template' do
      get :index
      expect(response).to render_template(:index)
    end
    it 'should render all climbs in json' do
      get :index, format: :json
      expect(response.body).to eq({ climbs: assigns(:climbs) }.to_json)
    end
    it 'should assign both climbs' do
      get :index
      expect(assigns(:climbs)).to include first_climb, second_climb
    end
  end

  describe 'POST create' do
    it 'should create a climb' do
      expect {
        post :create, climb: { date: DateTime.now, rating: '5.10a' }
      }.to change { Climb.count }.by(1)
    end
    it 'should create a climb from data' do
      post :create, climb: { date: DateTime.now, rating: '5.10a' }
      expect(Climb.last.date).to_not be_nil
    end
    it 'should create a climb from data' do
      post :create, climb: { date: DateTime.now, rating: '5.10a' }
      expect(Climb.last.rating).to eq '5.10a'
    end
    context 'html' do
      it 'should redirect to index' do
        post :create, climb: { date: DateTime.now, rating: '5.10a' }
        expect(response).to redirect_to(action: :index)
      end
    end
    context 'json' do
      it 'should render result' do
        post :create, climb: { date: DateTime.now, rating: '5.10a' }, format: :json
        expect(response.body).to eq Climb.last.to_json
      end
    end
  end
end
